# Description

Ansible role to rsync files between hosts.

This role does nothing unless `local_job_scripts` is defined. Also
`local_user` and `local_job_logs` must be defined.

    local_job_scripts: '{{ ... }}  # same as metws_job_scripts for data_servers
    local_job_logs: '{{ ... }}'    # same as metws_log_dir_jobs for data_servers
    local_user: "{{ ... }}"        # same as metws_user for data_servers

This role depends on the role `metno-devcoop-jobs-virtualenvs-base`
and requires it to be called with

    - include_role:
        name: metno-devcoop-jobs-virtualenvs-base
      vars:
        metws_job_scripts: '{{ local_job_scripts }}'
        metws_log_dir_jobs: '{{ local_job_logs }}'
        metws_user: '{{ local_user }}'
      when: local_job_scripts is defined and local_job_scripts

The main configuration for this role is the variable `rsync_pull_jobs`
which should be set correpsonding to this template:

    rsync_pull_jobs:
      - name: get_them
        host: "{{ remote host or ip }}"
        user: "{{ local_user }}"
        # ssh_host_key: "{{ lookup('file', '..._ssh_hostkey.pub') }}" # optional, no default
        # ssh_key_path: "/home/{{ local_user }}/.ssh/id_pull"         # optional, no default
        # cron_minute: "*/15"  # optional
        # cron_hour: "*"       # optional
        # cron_disabled: True  # optional
        fetch:
          - name: small_files
            src: "/there/on/remote/"
            dst: "/copied/to/here/"
            # delete:         # optional, if True, then run rsync with --delete
            # disabled: True  # optional

If `node_exporter_textfile_dir` is defined, some metrics will be
produced.
